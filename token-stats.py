#!/usr/bin/env python3
"""
Plot a histogram of the number of tokens in the items of a dataset.
The dataset is in jsonl format.
"""
import sys
import json
import matplotlib.pyplot as plt

from transformers import AutoTokenizer


def run(jsonl_path):
    tokenizer = AutoTokenizer.from_pretrained(
        "WizardLM/WizardCoder-Python-7B-V1.0",
        padding_side="right",
        use_fast=False,  # Fast tokenizer giving issues.
        trust_remote_code=False,
        use_auth_token=True,
    )
    lens = []
    with open(jsonl_path, "r") as f:
        for line in f:
            item = json.loads(line)
            tokens = tokenizer(item["output"])
            lens.append(len(tokens["input_ids"]))

    plt.hist(lens, bins=10)
    plt.show()


if __name__ == "__main__":
    run(sys.argv[1])
