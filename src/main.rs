use crate::stripper::FnBodyStripper;
use serde::Serialize;
use std::path::Path;
use syn::visit_mut::VisitMut;
use syn::{File, Item};

pub mod stripper;

fn main() {
    let dir_path = std::env::args().nth(1).unwrap_or("src".to_string());
    // iterate recursively over all files in the directory
    let walker = walkdir::WalkDir::new(&dir_path)
        .into_iter()
        .filter_entry(|e| e.file_name() != "target");
    for entry in walker {
        let entry = entry.unwrap();

        if entry.file_type().is_file() {
            let path = entry.path();
            if path.extension().map(|s| s == "rs").unwrap_or(false) {
                let relative_path = path.strip_prefix(&dir_path).unwrap();
                process_file(path, relative_path);
            }
        }
    }
}

fn process_file(path: &Path, relative_path: &Path) {
    let src = std::fs::read_to_string(path).unwrap();
    let file: File = syn::parse_str(&src).unwrap();

    // all use statements, and all items with no bodies
    let mut stripper = FnBodyStripper::new(None);
    let mut file_clone = file.clone();
    stripper.visit_file_mut(&mut file_clone);
    emit_file(relative_path, &file_clone);

    // each item with one body expanded at a time
    for item in file.items {
        if let Item::Mod(_item) = item {
            // TODO handle modules
        } else {
            process_item(relative_path, &item);
        }
    }
}

#[derive(Serialize)]
struct DatasetLine {
    input: String,
    output: String,
}

fn process_item(path: &Path, item: &Item) {
    let mut stripper = FnBodyStripper::new(None);
    stripper.visit_item_mut(&mut item.clone());
    let idents = stripper.idents;

    for (i, _ident) in idents.iter().enumerate() {
        let mut stripper = FnBodyStripper::new(Some(i));
        let mut mut_item = item.clone();
        stripper.visit_item_mut(&mut mut_item);
        let focus_file = File {
            shebang: None,
            attrs: vec![],
            items: vec![mut_item],
        };
        emit_file(path, &focus_file);
    }
}

fn emit_file(path: &Path, focus_file: &File) {
    let output = format!(
        "PATH: {}\n\n{}\n-----\n",
        path.display(),
        prettyplease::unparse(&focus_file)
    );
    let json_line = DatasetLine {
        input: "".to_string(),
        output,
    };
    let json = serde_json::to_string(&json_line).unwrap();

    println!("{}", json);
}
