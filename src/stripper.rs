use syn::visit_mut::VisitMut;
use syn::{Block, Ident, ImplItemFn, ItemFn, TraitItemFn};

/// Eliminate function bodies
pub struct FnBodyStripper {
    pub idents: Vec<Ident>,
    expand_body: Option<usize>,
}

impl FnBodyStripper {
    pub fn new(expand_body: Option<usize>) -> Self {
        FnBodyStripper {
            idents: vec![],
            expand_body,
        }
    }

    fn elide(&mut self, block: &mut Block, ident: &Ident) {
        if Some(self.idents.len()) != self.expand_body {
            *block = syn::parse_quote!({ XXX });
        }
        self.idents.push(ident.clone());
    }
}

impl VisitMut for FnBodyStripper {
    fn visit_impl_item_fn_mut(&mut self, i: &mut ImplItemFn) {
        self.elide(&mut i.block, &i.sig.ident);
    }

    fn visit_item_fn_mut(&mut self, i: &mut ItemFn) {
        self.elide(&mut *i.block, &i.sig.ident);
    }

    fn visit_trait_item_fn_mut(&mut self, i: &mut TraitItemFn) {
        if let Some(ref mut b) = i.default {
            self.elide(b, &i.sig.ident);
        }
    }
}
