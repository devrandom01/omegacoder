# omegacoder

## Usage

Create a dataset from a Rust code repository:

```shell
cargo run -- DIR > dataset.jsonl
```

Check the distribution of record sizes (in number of tokens) in the dataset:

```shell
./token-stats.py dataset.jsonl
```
